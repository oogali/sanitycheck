#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'net/http'
require 'uri'
require 'digest/sha1'
require 'digest/sha2'
require 'digest/md5'
require 'yaml'

DEFAULT_ISO_PARTIAL_RANGE = [ 32768, 65535 ]

module CentOS
  class ReleaseCheck
    attr_reader :release, :path, :checks
    @http

    def initialize(release, http, path)
      raise "Must initialize using hash of (release => release_checks)" if !release or !release.is_a?(Hash)
      @release = release.keys.first
      @checks =  release[@release]
      @http = http
      @path = path
    end

    def do_size_check(file, expected_size)
      size = get_file_size(file)
      passed = size == expected_size

      { :passed => passed, :value => size }.merge(passed ? {} : { :expected => expected_size })
    end

    def do_digest_check(file, digest_method, expected_digest, range = nil)
      digest = get_file_digest(file, digest_method, range)
      passed = digest == expected_digest

      { :passed => passed, :method => digest_method, :value => digest }.merge(passed ? {} : { :expected => expected_digest })
    end

    def do_repo_check(xml)
      to_check = []
      passed = true
      base_repo_path = File.dirname(xml)
      results = { :passed => false, :values => {} }

      repodoc = Nokogiri::XML self.get_file_contents(xml)
      repodoc.xpath('/repo:repomd/repo:data', { 'repo' => 'http://linux.duke.edu/metadata/repo' }).each do |entry|
        repo_filename = File.basename entry.xpath('repo:location', { 'repo' => 'http://linux.duke.edu/metadata/repo' }).attr('href').text
        repo_filepath = File.join(base_repo_path, repo_filename)
        to_check.push repo_filepath
      end

      to_check.each do |filename|
        expected = File.basename(filename).split('-', 2).first
        digest = get_file_digest(filename, :sha256)
        passed &= (digest == expected)

        results[:values][filename] = { :passed => digest == expected, :method => :sha256, :value => digest }.merge(digest == expected ? {} : { :expected => expected })
      end

      results[:passed] = passed
      results
    end

    def relative_path(path)
      File.join(@path, @release, path)
    end

    def file_exists?(path)
      @http.request_head(self.relative_path(path)).is_a?Net::HTTPOK
    end

    def is_release_hosted?
      self.file_exists? '/'
    end

    def get_file_size(filename)
      reply = @http.request_head(self.relative_path(filename))
      reply.content_length
    end

    def get_file_contents(filename, range = nil)
      req = Net::HTTP::Get.new(self.relative_path(filename))
      req.set_range(range.first, (range.last - range.first) + 1) if range
      reply = @http.request(req)

      # some servers will return more data than we've asked for, truncate it
      # looks like apache httpd does 64KB as minimum size for a range request?
      reply.body
    end

    def get_file_digest(filename, method, range = nil)
      digest_provider = nil

      case method
        when :md5
          digest_provider = Digest::MD5.new
        when :sha1
          digest_provider = Digest::SHA1.new
        when :sha256
          digest_provider = Digest::SHA2.new(256)
        when :sha384
          digest_provider = Digest::SHA2.new(384)
        when :sha512
          digest_provider = Digest::SHA2.new(512)
        else
          raise "Unsupported digest provider requested: #{method}"
      end

      (digest_provider << self.get_file_contents(filename, range)).to_s
    end

    def check
      results = self.is_release_hosted? ? { :passed => true } : nil
      return nil if !results

      p @release
      @checks.each do |file, args|
        file = file.gsub(/##VERSION##/, @release)
        results[file] = self.file_exists?(file) ? {} : nil
        next if !results[file]

        args.each do |check_name, check_val|
          case check_name
            when :size
              results[file][:size] = do_size_check(file, check_val)

            when :digest
              range = check_val[:range] || nil
              results[file][:digest] = do_digest_check(file, check_val[:method], check_val[:expected], range)

            when :repo
              results[file][:repo] = do_repo_check(file)

          end
        end
      end

      results
    end
  end

  class SanityCheck
    attr_accessor :checks, :uri
    @http

    def initialize(base_uri, checks)
      raise 'Invalid check bundle passed' if !checks or !checks.is_a? Hash
      @checks = checks

      uri = URI.parse(base_uri + '/') rescue nil
      raise 'Invalid URL' if !uri
      @uri = uri

      @http = Net::HTTP.new(@uri.host, @uri.port)
    end

    def is_valid_mirror?
      @http.request_head(@uri.path).is_a?Net::HTTPOK
    end

    def releases
      @checks.keys
    end

   def check(releases = nil)
      return nil if !is_valid_mirror?

      results = {}
      releases.reject! { |release| self.releases.include? release } if releases
      releases = self.releases if !releases or releases.empty?

      releases.each do |release|
        r = CentOS::ReleaseCheck.new({ release => @checks[release] }, @http, @uri.path)
        results[release] = r.check
      end

      results.keys.each do |release|
        passed = true

        results[release].keys.each do |file|
          next if file == 'passed'

          if !results[release][file]
            passed = false
          else
            passed &= results[release][file][:passed]
          end

          break if !passed
        end

        results[release][:passed] = passed
      end

      results
    end
  end
end

if __FILE__ == $0
  if ARGV.length != 2
    puts '__FILE__ [CentOS Checks DB File] [CentOS Mirror URI]'
    exit 1
  end

  dbfile = ARGV.shift
  mirror = ARGV.shift
  checks = YAML.load_file(dbfile)

  c = CentOS::SanityCheck.new(mirror, checks)
  puts c.check.to_yaml
end
