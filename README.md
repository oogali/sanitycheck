sanitycheck - a new way to check your CentOS mirror for validity
----------------------------------------------------------------

sanitycheck is a Ruby script designed to perform a set of checks (defined in a YAML file as input), against a CentOS mirror.

It's capable of performing the following comparisons on a remote mirror:

- size comparison

- full and partial digest comparisons (currently only SHA-256 is supported)

- repo enumeration


Why partial digest comparison?
==============================

Well, say you want to make sure that spiffy new ISO on the remote server is correct. But, you want to do it without pulling a 4.7GB DVD down over your satellite link onto your slow Celery 2 333MHz desktop.

A partial digest is computed on a range of bytes specified in the database. So instead of grabbing, the entire ISO, we only pull 32KB (current default) and run our digest operations against that.

This check isn't meant to be a replacement for a full digest check, but instead a way to blast through a list of mirrors quickly.


What is 'repo enumeration'?
===========================

Each CentOS release contains a number of Yum repository metadata XML files that in turn, specify other metadata files and their digests.

sanitycheck will grab each specified XML file, walk its contents to find the relevant metadata and digests, then grab the entire contents of said metadata and do a digest comparison. (Currently, CentOS uses SHA-256 digests, so we default to that).

If any individual file fails, then the parent metadata file fails its checks.


Okay, how do I use it?
======================

You can use it as a Ruby class, or from the command line.

To incorporate it into your existing Ruby code...

``` ruby
require 'sanitycheck'

checks = YAML.load_file('/path/to/checks/you/wish/to/run')
sanity = CentOS::SanityCheck.new(checks, 'http://your.centos.mirror.tld/path/to/CentOS/releases/')
results = sanity.check
```

Or, to use it from the command line...

```
./sanitycheck.rb /path/to/checks http://your.centos.mirror.tld/path/
```

I've included two checks, to get you started.

* checks/centos-6.0-all.yml: A check for all torrents, repo metadata, and ISOs for both i386 and x86_64 architectures

* checks/centos-6.0-no-isos.yml: A check for the above, but *without* ISOs


How do I parse the output?
==========================

The results of each check are output in YAML format.

So if you're using Ruby, you can read it into a Hash with the following:

``` ruby
require 'yaml'   # for you 1.8.7 folks

results = YAML.load_file('/path/to/results/file')
```


TODO
====
* Write code to generate a database file from an existing mirrors

* Needs a lot more code comments... and cowbell

* Catch those exceptions if you can


Author
======
Omachonu Ogali / oogali@gmail.com / @oogali
